<?php

// urls with prefix `company1`
Route::group('company1', function() {

	// will work in case of GET request with url `/company1/1/google-analytics/2`
	Route::get('/{site}/google-analytics/{date}', function(RouteParam $site, RouteParam $date) {
		$site->numeric();
		$date->numeric();
	})->to('client1::google'); //will work Client1Controller's googleAction($site, $date)

	// will work in case of POST request with url `/company1/1/daily-updates/2`
	Route::get('/{site}/daily-updates/{day}', function(RouteParam $site, RouteParam $day) {
		$site->numeric();
		$day->numeric();
	})->to('client1::database'); //will work Client1Controller's databaseAction($site, $day)

});

// urls with prefix `company2`
Route::group('company2', function() {

	// will work in case of GET request with url `/company2/1/yandex-metrica/2`
	Route::get('/{site}/yandex-metrica/{date}', function(RouteParam $site, RouteParam $date) {
		$site->numeric();
		$date->numeric();
	})->to('client2::yandex'); //will work Client2Controller's yandexAction($site, $date)

	// will work in case of POST request with url `/company2/1/daily-updates/2`
	Route::post('/{site}/daily-updates/{day}', function(RouteParam $site, RouteParam $day) {
		$site->numeric();
		$day->numeric();
	})->to('client2::database'); //will work Client2Controller's databaseAction($site, $day)

});
