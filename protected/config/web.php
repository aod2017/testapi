<?php

return [
	'basePath' => __DIR__,
	'params' => require 'params.php',
	'databases' => [
		'database1' => [
			'host' => 'localhost',
			'username' => 'root',
			'password' => '',
			'database' => 'database1'
		],
		'database2' => [
			'host' => 'localhost',
			'username' => 'root',
			'password' => '',
			'database' => 'database2'
		],
	],
	'routes' => function() {
		require_once 'routes.php';
	}
];