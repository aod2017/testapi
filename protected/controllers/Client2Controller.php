<?php

class Client2Controller extends Controller
{
	public function beforeAction()
	{
		$this->setDb(App::$data->databases['database2']);
	}

	public function yandexAction($site, $date)
	{
		// call to yandex metrica api using $date filter
		return [
			'day1' => 'some data from api response',
			'day2' => 'some data from api response',
			'day3' => 'some data from api response',
			'date' => $date,
			'site' => $site
		];
	}

	public function databaseAction($site, $day)
	{
		$data = [
			'hour1' => 'some data from database',
			'hour2' => 'some data from database',
			'hour3' => 'some data from database',
			'day' => $day,
			'site' => $site
		];
		/*
		$query = $this->database->prepare('select * from data where day = :day');
		$query->execute(['day' => $day]);
		$data = $query->fetchAll();
		*/
		return $data;
	}
}