<?php

class Redirect implements Processor
{
	private $url;

	public function __construct($url)
	{
		if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
			$this->url = $url;
		} else {
			$this->url = sprintf('%s%s', App::$data->baseUrl, trim($url, '/'));
		}
	}

	public function process()
	{
		header(sprintf('location: %s', $this->url));die;
	}
}