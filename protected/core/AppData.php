<?php

class AppData
{
	/**
	 * @var array
	 */
	public $params;

	/**
	 * @var string
	 */
	public $baseUrl;

	/**
	 * @var array
	 */
	public $routes;

	/**
	 * @var string
	 */
	public $webDir;

	/**
	 * @var string
	 */
	public $appDir;

	/**
	 * @var Request
	 */
	public $request;

	/**
	 * @var Request
	 */
	public $databases;
}