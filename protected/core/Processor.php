<?php

interface Processor
{
	public function process();
}