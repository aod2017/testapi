<?php

// comment this in production
define('ENV', 'dev');

$config = require(__DIR__ . '/../protected/config/web.php');
require(__DIR__ . '/../protected/core/App.php');
(new App($config))->start();